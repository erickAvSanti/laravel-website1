window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
	window.Popper = require('popper.js').default;
	window.jQuery = window.jQuery = require('jquery');

	window.axios = require('axios');
	window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
} catch (e) {
	alert('Missing Popper, jQuery or Axios components required');
}

import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

import Swal from 'sweetalert2'
window.Swal = require('sweetalert2')

import AdminPages from './components/AdminPages.vue'
import AdminPagesForm from './components/AdminPagesForm.vue'

import { Chrome } from 'vue-color'
Vue.component('chrome-picker', Chrome)

const admin_pages = new Vue({
	el: '#admin_pages',
	components:{
		AdminPages,
		AdminPagesForm,
	},
});

jQuery(window).ready(function(evt){
	jQuery("#app-left-btn").click(function(evt){
		jQuery("#panel-left").toggleClass("visible")
	})
	jQuery("#dismiss-left").click(function(evt){
		jQuery("#panel-left").toggleClass("visible")
	})
	jQuery("#app-right-btn").click(function(evt){
		jQuery("#panel-right").toggleClass("visible")
	})
	jQuery("#dismiss-right").click(function(evt){
		jQuery("#panel-right").toggleClass("visible")
	})
})