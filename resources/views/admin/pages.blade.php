@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">Páginas</div>

				<div class="card-body">
					@if (session('status'))
						<div class="alert alert-success" role="alert">
							{{ session('status') }}
						</div>
					@endif
					<div id="admin_pages">
						<admin-pages :url_form_new="`{{route('admin_page_new')}}`" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
