@extends('layouts.app')

@section('content')

<div class="app-container">
	<div id="app-left-btn" class="btn-primary btn">
		<i class="fa fa-arrow-right"></i>
	</div>
	<div id="app-right-btn" class="btn-primary btn">
		<i class="fa fa-arrow-left"></i>
	</div>
	<div class="panels">
		<div id="panel-left">
				<div class="card bg-custom1">
					<div class="card-header">
						<div class="btn btn-default btn-dismiss" id="dismiss-left">x</div>
						Menú
					</div>
					<div class="card-body">
						@include('layouts.admin.menu')
					</div>
				</div>
		</div>
		<div id="panel-center">
				<div class="card">
					<div class="card-header">Página</div>
					<div class="card-body">
						<div id="admin_pages">
							<admin-pages-form 
								v-bind:ws_create="`{{route('admin_page_ws_create')}}`" 
								v-bind:ws_update="`{{route('admin_page_ws_update',999)}}`" 
								v-bind:ws_delete="`{{route('admin_page_ws_delete',999)}}`" 
							/>
						</div>
					</div>
				</div>
		</div>
		<div id="panel-right">
				<div class="card bg-success">
					<div class="card-header">
						<div class="btn btn-default btn-dismiss-success" id="dismiss-right">x</div>
						Opciones de Página
					</div>
					<div class="card-body">
					</div>
				</div>
		</div>
	</div>
</div>
@endsection
