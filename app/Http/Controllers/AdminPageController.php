<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminPageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.pages');
    }
    public function form_new()
    {
        return view('admin.page_form');
    }
    public function form_edit($id)
    {
        return view('admin.page_form',['id' => $id]);
    }
    public function create(Request $request){
      return response()->json($request->all());
    }
    public function update(Request $request,$id){
      return response()->json($request->all());
    }
    public function delete($id){
      return response()->json(['id' => $id]);
    }
}
