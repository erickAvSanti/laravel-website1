<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();
Route::group(['prefix' => 'adm'], function() {
	Route::get('/panel', 'HomeController@index')->name('home');
	Route::group(['prefix' => 'pages'], function() {
		Route::get('/', 'AdminPageController@index')->name('admin_pages');
		Route::group(['prefix' => 'form'], function() {
			Route::get('/', 'AdminPageController@form_new')->name('admin_page_new');
			Route::get('/{id}', 'AdminPageController@form_edit')->name('admin_page_edit');
		});
	});


  Route::group(['prefix' => 'ws'], function() {
    Route::group(['prefix' => 'pages'], function() {

      Route::post('/', 'AdminPageController@create')->name('admin_page_ws_create');

      Route::put('/{id}', 'AdminPageController@update')->name('admin_page_ws_update')->where('id','\d+');

      Route::delete('/{id}', 'AdminPageController@delete')->name('admin_page_ws_delete')->where('id','\d+');
    });
    
  });
});

